#Flotype Bridge for Java
Flotype Bridge is a unified messaging system that allows you to easily build cross-language services to share data and realtime updates among your servers and your clients

##Installation
Quick install: Using [Apache Maven](http://maven.apache.org/), include the Flotype Bridge artifact in your `pom.xml` file

    <groupId>com.flotype.bridge</groupId>
    <artifactId>bridge-java</artifactId>
    <version>0.0.1-SNAPSHOT</version>

JAR install without Maven: A JAR file containing Flotype Bridge and all
depencies is available from
[Flotype](http://cloud.flotype.com/bridge.jar)

Source install: 

Clone this repository using `git clone git@bitbucket.org:flotype/bridge-java.git` and install using `mvn install` for Maven users. 

###Dependencies
This library has no external dependencies.

##Documentation and Support
* API Reference: http://flotype.com/docs/api/java/
* Getting Started: http://www.flotype.com/docs/gettingstarted/java/
* About Flotype and Flotype Bridge: http://www.flotype.com/

The `examples` directory of this library contains sample applications for Flotype Bridge.

Support is available in #flotype on Freenode IRC or the Flotype Bridge Google Group.


##License
Flotype Bridge is made available under the MIT/X11 license. See LICENSE file for details.

